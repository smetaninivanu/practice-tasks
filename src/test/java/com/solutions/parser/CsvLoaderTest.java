package com.solutions.parser;

import com.solutions.di.Application;
import com.solutions.di.ApplicationContext;
import com.solutions.model.Gender;
import com.solutions.model.Human;
import com.solutions.model.contracts.Contract;
import com.solutions.model.contracts.DigitalTVContract;
import com.solutions.model.contracts.MobileContract;
import com.solutions.model.contracts.WiredInternetContract;
import com.solutions.persistence.ContractRepository;
import com.solutions.sort.ISorter;
import com.solutions.sort.MergeSorter;
import com.solutions.validator.model.ValidationResult;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CsvLoaderTest {

  private static ApplicationContext app;
  private static ContractRepository repository;

  @BeforeAll
  static void initializeContext() {
    app = Application.run("com.solutions", new HashMap<>(Map.of(ISorter.class, MergeSorter.class)));
  }

  @BeforeEach
  void initialize() {
    repository = app.getObject(ContractRepository.class);
  }

  @Test
  void loadGood() {
    var result = app.getObject(CsvLoader.class).load("contracts-test.csv", repository);

    assertEquals(ValidationResult.OK, result.getResult());

    Human human1 = new Human(
      1,
      "Иванов Иван Иванович",
      LocalDate.of(2001, 7, 12),
      Gender.MALE,
      "2001 342769"
    );

    Human human2 = new Human(
      2,
      "Иванов Иван Иванович",
      LocalDate.of(2001, 7, 12),
      Gender.MALE,
      "2001 342766"
    );

    Contract contract1 = new MobileContract(
      1,
      1,
      LocalDateTime.of(2021, 8, 1, 8, 24),
      LocalDateTime.of(2022, 2, 1, 8, 24),
      human1,
      100,
      100,
      200
    );

    Contract contract2 = new WiredInternetContract(
      2,
      2,
      LocalDateTime.of(2021, 8, 1, 8, 24),
      LocalDateTime.of(2022, 2, 1, 8, 24),
      human2,
      100
    );

    DigitalTVContract contract3 = new DigitalTVContract(
      3,
      3,
      LocalDateTime.of(2021, 8, 1, 8, 24),
      LocalDateTime.of(2022, 2, 1, 8, 24),
      human1
    );

    contract3.add("MTV");
    contract3.add("BBC One");
    contract3.add("NatGeoWild");


    assertEquals(contract1, repository.getById(1, MobileContract.class));
    assertEquals(contract2, repository.getById(2, WiredInternetContract.class));
    assertThat(contract3)
      .usingRecursiveComparison()
      .isEqualTo(repository.getById(3, DigitalTVContract.class));
  }

  @Test
  void loadBad() {
    var result = app.getObject(CsvLoader.class).load("contracts-test-bad.csv", repository);

    assertEquals(ValidationResult.WARN, result.getResult());

    Object[] warnings = {
      "startDate is later than endDate;",
      "passportNumber is blank;",
      "birthday is null;",
      "gender is null or not equals М, Ж or О;",
      "startDate is later than endDate;",
      "contractNumber is incorrect;"
    };

    assertArrayEquals(warnings, result.getWarnings().toArray());
  }
}
