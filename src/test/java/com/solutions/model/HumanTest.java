package com.solutions.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Period;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {

  private static Human human;

  @BeforeAll
  static void initialize() {
    human = new Human(
      42,
      "Иванов Иван Иванович",
      LocalDate.of(2001, 8, 10),
      Gender.MALE,
      "2021 976125"
    );
  }

  @Test
  void getAge() {
    assertEquals(
      Period.between(human.getBirthday(), LocalDate.now()).getYears(),
      human.getAge()
    );
  }

  @Test
  void changeBirthdayAndGetAge() {
    human.setBirthday(LocalDate.of(2005, 3, 14));

    assertEquals(
      Period.between(human.getBirthday(), LocalDate.now()).getYears(),
      human.getAge()
    );
  }
}
