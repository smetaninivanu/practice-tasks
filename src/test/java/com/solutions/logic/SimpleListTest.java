package com.solutions.logic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleListTest {

  private static SimpleList<String> strings;

  @BeforeEach
  void initialize() {
    strings = new SimpleList<>();
  }

  @Test
  void size() {
    assertEquals(0, strings.size());

    strings.add("Test");
    assertEquals(1, strings.size());
  }

  @Test
  void isEmpty() {
    assertTrue(strings.isEmpty());
  }

  @Test
  void addAndGet() {
    strings.add("Test");
    assertEquals("Test", strings.get(0));
  }

  @Test
  void addByIndex() {
    assertThrows(IndexOutOfBoundsException.class, () -> strings.add(5, "Test"));

    strings.add(0, "Test_0");
    strings.add(1, "Test_1");
    strings.add(2, "Test_2");
    strings.add(1, "Test_3");
    assertArrayEquals(new String[]{"Test_0", "Test_3", "Test_1", "Test_2"}, strings.toArray());

    strings.remove(1);
    strings.remove(2);
    assertArrayEquals(new String[]{"Test_0", "Test_1"}, strings.toArray());
  }

  @Test
  void set() {
    assertThrows(IndexOutOfBoundsException.class, () -> strings.set(5, "Test"));

    assertThrows(IndexOutOfBoundsException.class, () -> strings.set(0, "Test"));

    strings.add(0, "Test_0");
    strings.add(1, "Test_1");
    strings.add(2, "Test_2");
    strings.set(1, "Test_3");
    assertArrayEquals(new String[]{"Test_0", "Test_3", "Test_2"}, strings.toArray());
  }

  @Test
  void remove() {
    assertFalse(strings.remove("Test_2"));

    strings.add(0, "Test_0");
    strings.add(1, "Test_1");
    strings.add(2, "Test_2");

    strings.remove("Test_0");
    assertArrayEquals(new String[]{"Test_1", "Test_2"}, strings.toArray());

    strings.remove("Test_2");
    assertArrayEquals(new String[]{"Test_1"}, strings.toArray());

    strings.remove("Test_1");
    assertArrayEquals(new String[]{}, strings.toArray());
  }

  @Test
  void removeByIndex() {
    assertThrows(IndexOutOfBoundsException.class, () -> strings.remove(5));
    assertThrows(IndexOutOfBoundsException.class, () -> strings.remove(0));

    strings.add(0, "Test_0");
    strings.add(1, "Test_1");
    strings.add(2, "Test_2");

    strings.remove(1);
    assertArrayEquals(new String[]{"Test_0", "Test_2"}, strings.toArray());

    strings.remove(1);
    assertArrayEquals(new String[]{"Test_0"}, strings.toArray());

    strings.remove(0);
    assertArrayEquals(new String[]{}, strings.toArray());
  }

  @Test
  void contains() {
    strings.add(0, "Test_0");
    strings.add(1, "Test_1");
    strings.add(2, "Test_2");

    assertTrue(strings.contains("Test_0"));

    assertFalse(strings.contains("Test_10"));
  }

  @Test
  void addAll() {
    strings.add(0, "Test_0");
    strings.add(1, "Test_1");
    strings.add(2, "Test_2");

    var strings2 = new SimpleList<String>();

    strings2.add("Test_10");
    strings2.add("Test_11");
    strings2.add("Test_12");

    strings.addAll(strings2);

    assertArrayEquals(new String[]{"Test_0", "Test_1", "Test_2", "Test_10", "Test_11", "Test_12"}, strings.toArray());
  }

  @Test
  void clear() {
    strings.add("Test_10");
    strings.add("Test_11");
    strings.add("Test_12");
    assertArrayEquals(new String[]{"Test_10", "Test_11", "Test_12"}, strings.toArray());

    strings.clear();
    assertArrayEquals(new String[]{}, strings.toArray());
  }
}
