package com.solutions.persistence;

import com.solutions.di.Application;
import com.solutions.di.ApplicationContext;
import com.solutions.exception.ContractNotFoundException;
import com.solutions.model.Gender;
import com.solutions.model.Human;
import com.solutions.model.contracts.Contract;
import com.solutions.model.contracts.DigitalTVContract;
import com.solutions.model.contracts.MobileContract;
import com.solutions.model.contracts.WiredInternetContract;
import com.solutions.sort.BubbleSorter;
import com.solutions.sort.ISorter;
import com.solutions.sort.MergeSorter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ContractRepositoryTest {

  private static ApplicationContext app;
  private ContractRepository repository;
  private Human human;

  static void initializeContext(HashMap<Class, Class> ifc2ImplClass) {
    app = Application.run("com.solutions", ifc2ImplClass);
  }

  void createAndFillRepository(){
    repository = app.getObject(ContractRepository.class);

    human = new Human(
      42,
      "Иванов Иван Иванович",
      LocalDate.of(2001, 8, 10),
      Gender.MALE,
      "2021 976125"
    );

    repository.addContract(
      new MobileContract(
        1,
        1,
        LocalDateTime.now(),
        LocalDateTime.now(),
        human,
        1,
        1,
        1
      )
    );

    DigitalTVContract digitalTVContract = new DigitalTVContract(
      3,
      3,
      LocalDateTime.now(),
      LocalDateTime.now(),
      human
    );

    digitalTVContract.add("TV-3");
    digitalTVContract.add("BBC");
    digitalTVContract.add("NatGeoWild");
    digitalTVContract.add("National Geographic");

    repository.addContract(digitalTVContract);

    repository.addContract(
      new WiredInternetContract
        (
          1,
          1,
          LocalDateTime.now(),
          LocalDateTime.now(),
          human,
          100
        )
    );

    repository.addContract(
      new MobileContract(
        2,
        2,
        LocalDateTime.now(),
        LocalDateTime.now(),
        human,
        100,
        100,
        100
      )
    );
  }

  @BeforeAll
  static void initializeContext() {
    initializeContext(new HashMap<>(Map.of(ISorter.class, MergeSorter.class)));
  }

  @BeforeEach
  void initialize() {
    createAndFillRepository();
  }

  @Test
  void getById() {
    assertThrows(
      ContractNotFoundException.class,
      () -> repository.getById(0, MobileContract.class)
    );

    assertThrows(
      ContractNotFoundException.class,
      () -> repository.getById(-1, WiredInternetContract.class)
    );

    assertThrows(
      ContractNotFoundException.class,
      () -> repository.getById(4, DigitalTVContract.class)
    );
  }

  @Test
  void size() {
    assertEquals(3, repository.size());
  }

  @Test
  void deleteById() {
    assertThrows(ContractNotFoundException.class, () -> repository.deleteById(0));
    assertThrows(ContractNotFoundException.class, () -> repository.deleteById(-1));
    assertThrows(ContractNotFoundException.class, () -> repository.deleteById(4));
    assertEquals(3, repository.size());

    repository.deleteById(1);

    var contract3 = new DigitalTVContract(
      3,
      3,
      repository.getById(3, DigitalTVContract.class).getStartDate(),
      repository.getById(3, DigitalTVContract.class).getEndDate(),
      human
    );

    contract3.add("TV-3");
    contract3.add("BBC");
    contract3.add("NatGeoWild");
    contract3.add("National Geographic");

    var contract2 = new MobileContract(
      2,
      2,
      repository.getById(2, MobileContract.class).getStartDate(),
      repository.getById(2, MobileContract.class).getEndDate(),
      human,
      100,
      100,
      100
    );

    assertThrows(ClassCastException.class, () -> repository.getById(3, WiredInternetContract.class));

    assertThat(contract2)
      .isEqualTo(repository.getById(2, MobileContract.class));

    assertThat(contract3)
      .usingRecursiveComparison()
      .isEqualTo(repository.getById(3, DigitalTVContract.class));
  }

  @Test
  void addContract() {
    var contract5 = new WiredInternetContract(
      5,
      5,
      LocalDateTime.now(),
      LocalDateTime.now(),
      human,
      400
    );

    repository.addContract(contract5);

    assertThat(contract5)
      .isEqualTo(repository.getById(5, WiredInternetContract.class));
  }

  @Test
  void addContracts() {
    var contract5 = new MobileContract(
      5,
      5,
      LocalDateTime.now(),
      LocalDateTime.now(),
      human,
      100,
      100,
      100
    );

    var contract6 = new WiredInternetContract
      (
        6,
        6,
        LocalDateTime.now(),
        LocalDateTime.now(),
        human,
        250
      );

    repository.addContracts(new Contract[]{contract6, contract5});

    assertThat(contract5)
      .isEqualTo(repository.getById(5, MobileContract.class));

    assertThat(contract6)
      .isEqualTo(repository.getById(6, WiredInternetContract.class));
  }

  @Test
  void search() {
    var contract5 = new MobileContract(
      5,
      5,
      LocalDateTime.now(),
      LocalDateTime.now(),
      human,
      50,
      100,
      100
    );

    repository.addContract(contract5);

    assertEquals(2, repository.search(p -> p.getId() < 6, MobileContract.class).size());
    assertEquals(1, repository.search(p -> p.getMinutes() < 70, MobileContract.class).size());
  }

  @Test
  void sortBubble() {
    initializeContext(new HashMap<>(Map.of(ISorter.class, BubbleSorter.class)));

    sortFunc();
  }

  @Test
  void sortMerge() {
    sortFunc();
  }

  void sortFunc(){
    repository.sort(Comparator.comparingLong(Contract::getId));

    for (int i = 1; i <= repository.size() - 2; i++) {
      long id1 = repository.getById(i, Contract.class).getId();
      long id2 = repository.getById(i + 1, Contract.class).getId();

      assertThat(id1).isLessThan(id2);
    }
  }
}
