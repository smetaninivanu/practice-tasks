package com.solutions.persistence;

import com.solutions.di.annotation.Autowired;
import com.solutions.exception.ContractNotFoundException;
import com.solutions.logic.SimpleList;
import com.solutions.model.Human;
import com.solutions.model.contracts.Contract;
import com.solutions.sort.ISorter;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Predicate;

public class ContractRepository {

  /**
   * List of contracts.
   */
  private final SimpleList<Contract> contracts;

  @Autowired
  private ISorter sorter;

  /**
   * Constructs an empty ContractRepository.
   */
  public ContractRepository() {
    contracts = new SimpleList<>();
  }

  /**
   * Returns the contract with the specified id and element type.
   *
   * @param id   contract id
   * @param type type of contract in {@code contracts} list
   * @return contract
   * @throws ContractNotFoundException if contract with the specified id doesn't exist
   */
  public <T extends Contract> T getById(long id, Class<T> type) {
    var matchingContract = findContractById(id);

    if (matchingContract.isPresent()) {
      return type.cast(matchingContract.get());
    } else {
      throw new ContractNotFoundException(id);
    }
  }

  /**
   * Searches contracts satisfying the type {@code type} and the predicate {@code p}.
   *
   * @param p    predicate for which contracts are selected
   * @param type type of contract in {@code contracts} list
   */
  public <T extends Contract> SimpleList<T> search(Predicate<T> p, Class<T> type) {
    var result = new SimpleList<T>();

    for (Contract contract : contracts) {

      if (contract.getClass().equals(type)) {
        T castedContract = (T) contract;

        if (p.test(castedContract)) {
          result.add(castedContract);
        }
      }
    }

    return result;
  }

  /**
   * Sorts the contracts list by the specified comparator and sorter implementation.
   *
   * @param comparator comparator by which sorting will be performed
   */
  public void sort(Comparator<Contract> comparator) {
    sorter.sort(contracts, comparator);
  }

  /**
   * Removes the contract with the specified id.
   *
   * @param id contract id
   * @throws ContractNotFoundException if id doesn't exist
   */
  public void deleteById(long id) {
    var matchingContract = findContractById(id);

    if (matchingContract.isPresent()) {
      contracts.remove(matchingContract.get());
    } else {
      throw new ContractNotFoundException(id);
    }
  }

  /**
   * Appends contract to the contracts list.
   * If contract with the same id is already exists, it will be replaced with the given contract.
   * If contract id doesn't exist (equals 0), value is selected from the first available id.
   *
   * @param contract contract to be inserted to the contracts list
   */
  public <T extends Contract> void addContract(T contract) {
    setHumanId(contract.getOwner());

    if (contract.getId() == 0) {
      SimpleList<Long> idList = Arrays.stream(this.contracts.toArray())
        .map(Contract.class::cast)
        .map(Contract::getId)
        .collect(SimpleList::new, SimpleList::add, SimpleList::addAll);

      long id = idList.size() == 0 ? 1 : idList.get(idList.size() - 1);

      while (idList.contains(id)) {
        id++;
      }

      contract.setId(id);
    } else {
      Contract[] matchingContracts = Arrays.stream(this.contracts.toArray())
        .map(Contract.class::cast)
        .filter(c -> c.getId() == contract.getId())
        .toArray(Contract[]::new);

      if (matchingContracts.length == 1) {
        this.contracts.set(this.contracts.indexOf(matchingContracts[0]), contract);
        return;
      }
    }

    this.contracts.add(contract);
  }

  /**
   * Appends elements of the contracts array to the contracts list.
   * If contract with the same is already exists, it will be replaced with the given contract
   *
   * @param contracts contracts array to be inserted to the contracts list
   */
  public void addContracts(Contract[] contracts) {
    Arrays.stream(contracts).forEach(this::addContract);
  }

  /**
   * Returns the size of contracts list.
   */
  public int size() {
    return contracts.size();
  }

  /**
   * Sets human id to human if it doesn't exist.
   * If a person with the same id is found this id is set.
   * Otherwise, the first available id is selected.
   *
   * @param human human at which the id is placed
   */
  private void setHumanId(Human human) {
    if (human.getId() == 0) {
      Human[] matchingOwners = Arrays.stream(this.contracts.toArray())
        .map(Contract.class::cast)
        .map(Contract::getOwner)
        .filter(o -> o.getPassportNumber().equals(human.getPassportNumber()))
        .toArray(Human[]::new);

      if (matchingOwners.length == 1) {
        human.setId(matchingOwners[0].getId());
      } else {
        SimpleList<Long> idList = Arrays.stream(this.contracts.toArray())
          .map(Contract.class::cast)
          .map(Contract::getOwner)
          .map(Human::getId)
          .collect(SimpleList::new, SimpleList::add, SimpleList::addAll);

        long id = idList.size() == 0 ? 1 : idList.get(idList.size() - 1);

        while (idList.contains(id)) {
          id++;
        }

        human.setId(id);
      }
    }
  }

  /**
   * Finds contract with the specified contract id.
   * Return empty optional if contract with the specified contract id doesn't exist.
   *
   * @param id contract id
   * @return optional of contract
   */
  private <T extends Contract> Optional<T> findContractById(long id) {
    Contract[] matchingContracts = Arrays.stream(this.contracts.toArray())
      .map(Contract.class::cast)
      .filter(c -> c.getId() == id)
      .toArray(Contract[]::new);

    if (matchingContracts.length == 1) return Optional.of((T) matchingContracts[0]);

    return Optional.empty();
  }
}
