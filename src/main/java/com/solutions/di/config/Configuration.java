package com.solutions.di.config;


import com.solutions.di.reflection.Reflections;

public interface Configuration {
  <T> Class<? extends T> getImplClass(Class<T> ifc);

  Reflections getScanner();
}
