package com.solutions.di;

import com.solutions.di.config.JavaConfiguration;
import com.solutions.di.factory.ObjectFactory;

import java.util.Map;
import java.util.logging.Logger;


public class Application {
  private static final Logger logger = Logger.getLogger(Application.class.getCanonicalName());

  public static ApplicationContext run(String packageToScan) {
    logger.info("Context was created");

    JavaConfiguration config = new JavaConfiguration(packageToScan);
    ApplicationContext context = new ApplicationContext(config);
    ObjectFactory objectFactory = new ObjectFactory(context);
    context.setFactory(objectFactory);
    return context;
  }

  public static ApplicationContext run(String packageToScan, Map<Class, Class> ifc2ImplClass) {
    JavaConfiguration config = new JavaConfiguration(packageToScan, ifc2ImplClass);
    ApplicationContext context = new ApplicationContext(config);
    ObjectFactory objectFactory = new ObjectFactory(context);
    context.setFactory(objectFactory);

    return context;
  }
}


