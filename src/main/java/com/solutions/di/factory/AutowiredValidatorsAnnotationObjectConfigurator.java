package com.solutions.di.factory;

import com.solutions.di.ApplicationContext;
import com.solutions.di.annotation.AutowiredValidators;
import com.solutions.logic.SimpleList;
import com.solutions.validator.ContractValidator;

import java.lang.reflect.Field;
import java.util.Set;

public class AutowiredValidatorsAnnotationObjectConfigurator implements ObjectConfigurator {
  @Override
  public void configure(Object t, ApplicationContext context) {
    for (Field field : t.getClass().getDeclaredFields()) {

      if (field.isAnnotationPresent(AutowiredValidators.class) && SimpleList.class.isAssignableFrom(field.getType())) {

        Set<Class<? extends ContractValidator>> implCommandsClasses = context
          .getConfig()
          .getScanner()
          .getSubTypesOf(ContractValidator.class);

        SimpleList<ContractValidator> contractValidators = implCommandsClasses
          .stream()
          .map(context::getObject)
          .collect(SimpleList::new, SimpleList::add, SimpleList::addAll);

        field.setAccessible(true);

        try {
          field.set(t, contractValidators);
        } catch (IllegalAccessException e) {
          throw new RuntimeException("Error with injecting to map");
        }
      }
    }
  }
}
