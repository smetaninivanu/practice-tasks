package com.solutions.di.factory;


import com.solutions.di.ApplicationContext;

public interface ObjectConfigurator {
  void configure(Object t, ApplicationContext context);
}
