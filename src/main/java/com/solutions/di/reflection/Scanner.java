package com.solutions.di.reflection;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Scanner {

  private static String getClassName(String classPath) {
//    Pattern pattern = Pattern.compile("(?:(?!\\.).)+$");
//    Matcher matcher = pattern.matcher(classPath.substring(0, classPath.length() - ".class".length()));
//    if (matcher.find()) {
//      classPath = classPath
//        .substring(matcher.start(), matcher.end());
//    }
    return classPath.substring(0, classPath.length() - ".class".length());
  }

  public static Set<Class<?>> findAllClasses(String packageToScan) {

    URL resource = Scanner.class
      .getClassLoader()
      .getResource(packageToScan.replace('.', '/'));

    String mainPath = String.valueOf(resource)
      .replaceAll("libs[\\w\\W]*!", "classes/java/main")
      .replaceAll("jar:|file:", "")
      .replaceAll("test-classes", "classes");

    File[] files = new File(mainPath).listFiles();
    Set<Class<?>> classes = new HashSet<>();

    for (File file : Objects.requireNonNull(files)) {
      classes.addAll(findInDirectory(file, packageToScan));
    }

    return classes;
  }

  private static Set<Class<?>> findInDirectory(File file, String packageToScan) {
    Set<Class<?>> classes = new HashSet<>();
    String classPath = packageToScan + '.' + file.getName();

    if (!file.isDirectory() && classPath.endsWith(".class")) {
      try {
        classes.add(Class.forName(getClassName(classPath)));
      } catch (ClassNotFoundException e) {
        throw new RuntimeException("Class not found");
      }
    } else if (file.isDirectory()) {
      for (File child : Objects.requireNonNull(file.listFiles())) {
        classes.addAll(findInDirectory(child, classPath));
      }
    }

    return classes;
  }
}
