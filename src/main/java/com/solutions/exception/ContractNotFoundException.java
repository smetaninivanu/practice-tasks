package com.solutions.exception;

public class ContractNotFoundException extends RuntimeException {

  public ContractNotFoundException(long id) {
    super("Contract with id =" + id + " not found.");
  }
}
