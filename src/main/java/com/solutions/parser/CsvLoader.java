package com.solutions.parser;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import com.solutions.di.annotation.AutowiredValidators;
import com.solutions.di.annotation.Singleton;
import com.solutions.logic.SimpleList;
import com.solutions.model.Gender;
import com.solutions.model.Human;
import com.solutions.model.contracts.Contract;
import com.solutions.model.contracts.DigitalTVContract;
import com.solutions.model.contracts.MobileContract;
import com.solutions.model.contracts.WiredInternetContract;
import com.solutions.persistence.ContractRepository;
import com.solutions.validator.ContractValidator;
import com.solutions.validator.model.ValidResultDto;
import com.solutions.validator.model.ValidationResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;

@Singleton
public class CsvLoader {

  private static final Logger logger = LogManager.getLogger(CsvLoader.class);

  @AutowiredValidators
  private SimpleList<ContractValidator> contractValidators;

  private SimpleList<String[]> parsedCsv;

  public ValidResultDto load(String pathToFile, ContractRepository contractRepository) {
    try {
      parsedCsv = oneByOne(pathToFile);
    } catch (CsvValidationException | IOException | URISyntaxException e) {
      e.printStackTrace();
    }

    var humans = parseOwner();
    var contracts = parseContract();

    ValidResultDto validationGlobalResult = new ValidResultDto();
    for (int i = 0; i < contracts.size(); i++) {
      Contract contract = contracts.get(i);
      contract.setOwner(humans.get(i));

      ValidResultDto validationResult = validateContract(contract);

      if (validationResult.getResult().equals(ValidationResult.OK)) {
        contractRepository.addContract(contract);
      } else {
        for (String warn : validationResult.getWarnings()) {
          logger.warn(warn);
        }

        validationGlobalResult.addAll(validationResult.getWarnings());
      }
    }

    return validationGlobalResult;
  }

  private ValidResultDto validateContract(Contract contract) {
    ValidResultDto resultDto = new ValidResultDto();
    for (ContractValidator validator : contractValidators) {
      var dto = validator.validContract(contract);

      resultDto.setResult(dto.getResult());
      resultDto.addAll(dto.getWarnings());
    }

    return resultDto;
  }

  private SimpleList<Human> parseOwner() {
    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    var result = new SimpleList<Human>();

    for (String[] string : parsedCsv) {
      if (string.length != 9) continue;

      Human human = new Human();

      human.setFullName(string[0]);
      human.setGender(Gender.fromName(string[1]).orElse(null));
      try {
        human.setBirthday(LocalDate.parse(string[2], dateFormatter));
      } catch (DateTimeParseException exception) {
        human.setBirthday(null);
      }
      human.setPassportNumber(string[3]);

      result.add(human);
    }

    return result;
  }

  private SimpleList<Contract> parseContract() {
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm dd.MM.yyyy");

    var result = new SimpleList<Contract>();

    for (String[] string : parsedCsv) {
      Contract contract = switch (string[7]) {
        case "Mobile" -> {
          MobileContract mobileContract = new MobileContract();

          var parameters = string[8].split(",");
          if (parameters.length == 3) {
            mobileContract.setMinutes(Integer.parseInt(parameters[0]));
            mobileContract.setSms(Integer.parseInt(parameters[1]));
            mobileContract.setTraffic(Integer.parseInt(parameters[2]));
          }

          yield mobileContract;
        }
        case "Wired" -> {
          WiredInternetContract wiredInternetContract = new WiredInternetContract();

          wiredInternetContract.setConnectionSpeed(Integer.parseInt(string[8]));

          yield wiredInternetContract;
        }
        case "DigitalTV" -> {
          DigitalTVContract digitalTVContract = new DigitalTVContract();

          SimpleList<String> channels = Arrays.stream(string[8].split(","))
            .collect(SimpleList::new, SimpleList::add, SimpleList::addAll);

          digitalTVContract.setChannels(channels);

          yield digitalTVContract;
        }
        default -> null;
      };

      if (contract == null) continue;

      try {
        contract.setContractNumber(Integer.parseInt(string[4]));
      } catch (NumberFormatException exception) {
        contract.setContractNumber(0);
      }
      try {
        contract.setStartDate(LocalDateTime.parse(string[5], dateTimeFormatter));
      } catch (DateTimeParseException exception) {
        contract.setStartDate(null);
      }
      try {
        contract.setEndDate(LocalDateTime.parse(string[6], dateTimeFormatter));
      } catch (DateTimeParseException exception) {
        contract.setEndDate(null);
      }

      result.add(contract);
    }

    return result;
  }

  private SimpleList<String[]> oneByOne(String pathToFile) throws CsvValidationException, IOException, URISyntaxException {
    Reader reader = Files.newBufferedReader(Paths.get(
      ClassLoader.getSystemResource(pathToFile).toURI()
    ));

    SimpleList<String[]> list = new SimpleList<>();

    CSVParser parser = new CSVParserBuilder()
      .withSeparator(';')
      .withIgnoreQuotations(true)
      .build();

    CSVReader csvReader = new CSVReaderBuilder(reader)
      .withSkipLines(1)
      .withCSVParser(parser)
      .build();

    String[] line;
    while ((line = csvReader.readNext()) != null) {
      if (line.length != 9) continue;

      list.add(line);
    }

    reader.close();
    csvReader.close();

    return list;
  }
}
