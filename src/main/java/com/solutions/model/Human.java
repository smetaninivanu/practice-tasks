package com.solutions.model;

import lombok.*;

import java.time.LocalDate;
import java.time.Period;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class Human {

  private long id;

  private String fullName;

  @Setter(value = AccessLevel.NONE)
  private LocalDate birthday;

  private Gender gender;

  private String passportNumber;

  @Setter(value = AccessLevel.NONE)
  private int age;

  public Human(
    long id,
    String fullName,
    LocalDate birthday,
    Gender gender,
    String passportNumber
  ) {
    this.id = id;
    this.fullName = fullName;
    this.birthday = birthday;
    this.gender = gender;
    this.passportNumber = passportNumber;
    setAge(this.birthday);
  }

  /**
   * Calculates the age by the given birthdate.
   *
   * @param birthday birthdate
   */
  private void setAge(LocalDate birthday) {
    if (birthday == null) {
      age = 0;
    } else {
      this.age = Period.between(birthday, LocalDate.now()).getYears();
    }
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday = birthday;
    setAge(this.birthday);
  }
}
