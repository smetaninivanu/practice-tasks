package com.solutions.model.contracts;

import com.solutions.model.Human;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class WiredInternetContract extends Contract {

  private int connectionSpeed;

  public WiredInternetContract(
    long id,
    long contractNumber,
    LocalDateTime startDate,
    LocalDateTime endDate,
    Human owner,
    int connectionSpeed
  ) {
    super(id, contractNumber, startDate, endDate, owner);

    this.connectionSpeed = connectionSpeed;
  }
}
