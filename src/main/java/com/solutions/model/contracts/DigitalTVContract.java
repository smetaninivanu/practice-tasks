package com.solutions.model.contracts;

import com.solutions.logic.SimpleList;
import com.solutions.model.Human;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class DigitalTVContract extends Contract {

  private SimpleList<String> channels;

  public DigitalTVContract(
    long id,
    long contractNumber,
    LocalDateTime startDate,
    LocalDateTime endDate,
    Human owner
  ) {
    super(id, contractNumber, startDate, endDate, owner);
    channels = new SimpleList<>();
  }

  public int size() {
    return channels.size();
  }

  public boolean add(String channel) {
    return channels.add(channel);
  }

  public boolean remove(String channel) {
    return channels.remove(channel);
  }

  public void clear() {
    channels.clear();
  }

  public String get(int index) {
    return channels.get(index);
  }

  public String set(int index, String channel) {
    return channels.set(index, channel);
  }
}
