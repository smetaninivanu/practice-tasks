package com.solutions.model.contracts;

import com.solutions.model.Human;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@ToString
@NoArgsConstructor
public abstract class Contract {

  private long id;

  private long contractNumber;

  private LocalDateTime startDate;

  private LocalDateTime endDate;

  private Human owner;
}
