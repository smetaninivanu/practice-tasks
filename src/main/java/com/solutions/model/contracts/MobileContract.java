package com.solutions.model.contracts;

import com.solutions.model.Human;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class MobileContract extends Contract {

  private int minutes;

  private int sms;

  private int traffic;

  public MobileContract(
    long id,
    long contractNumber,
    LocalDateTime startDate,
    LocalDateTime endDate,
    Human owner,
    int minutes,
    int sms,
    int traffic
  ) {
    super(id, contractNumber, startDate, endDate, owner);

    this.minutes = minutes;
    this.sms = sms;
    this.traffic = traffic;
  }
}
