package com.solutions.model;

import java.util.Objects;
import java.util.Optional;

public enum Gender {
  MALE(1, "M"),
  FEMALE(2, "F"),
  OTHER(3, "O");

  private final int id;
  private final String name;

  Gender(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public static Optional<Gender> fromId(Integer id) {
    if (id == null) {
      return Optional.empty();
    }

    for (var value : Gender.values()) {
      if (value.id == id) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  public static Optional<Gender> fromName(String name) {
    if (name == null) {
      return Optional.empty();
    }

    for (var value : Gender.values()) {
      if (Objects.equals(value.name, name)) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }
}
