package com.solutions.sort;

import com.solutions.logic.SimpleList;
import com.solutions.model.contracts.Contract;

import java.util.Comparator;

public class MergeSorter implements ISorter {

  private Comparator<Contract> comparator;

  private SimpleList<Contract> list;

  /**
   * Merges two sublists.
   *
   * @param left left sort bound
   * @param middle middle sort bound
   * @param right right sort bound
   */
  private void merge(int left, int middle, int right) {
    int n1 = middle - left + 1;
    int n2 = right - middle;

    SimpleList<Contract> leftPart = new SimpleList<>();
    SimpleList<Contract> rightPart = new SimpleList<>();

    for (int i = 0; i < n1; ++i)
      leftPart.add(i, list.get(left + i));
    for (int j = 0; j < n2; ++j)
      rightPart.add(j, list.get(middle + 1 + j));

    int i = 0;
    int j = 0;

    int k = left;
    while (i < n1 && j < n2) {
      if (comparator.compare(leftPart.get(i), rightPart.get(j)) <= 0) {
        list.set(k, leftPart.get(i));
        i++;
      } else {
        list.set(k, rightPart.get(j));
        j++;
      }
      k++;
    }

    while (i < n1) {
      list.set(k, leftPart.get(i));
      i++;
      k++;
    }

    while (j < n2) {
      list.set(k, rightPart.get(j));
      j++;
      k++;
    }
  }

  /**
   * Sorts list in {@code left} and {@code right} bounds.
   *
   * @param left left bound
   * @param right right bound
   */
  private void sort(int left, int right) {
    if (left < right) {
      int median = left + (right - left) / 2;

      sort(left, median);
      sort(median + 1, right);

      merge(left, median, right);
    }
  }

  @Override
  public void sort(SimpleList<Contract> list, Comparator<Contract> comparator) {
    this.comparator = comparator;
    this.list = list;

    sort(0, list.size() - 1);
  }
}
