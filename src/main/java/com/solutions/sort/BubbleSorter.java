package com.solutions.sort;

import com.solutions.logic.SimpleList;
import com.solutions.model.contracts.Contract;

import java.util.Comparator;

public class BubbleSorter implements ISorter {

  @Override
  public void sort(SimpleList<Contract> list, Comparator<Contract> comparator) {
    for (int i = 0; i < list.size() - 1; i++) {
      for (int j = 0; j < list.size() - i - 1; j++) {
        if (comparator.compare(list.get(j), list.get(j + 1)) > 0) {
          var temp = list.get(j);

          list.set(j, list.get(j + 1));
          list.set(j + 1, temp);
        }
      }
    }
  }
}
