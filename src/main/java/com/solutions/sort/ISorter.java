package com.solutions.sort;

import com.solutions.logic.SimpleList;
import com.solutions.model.contracts.Contract;

import java.util.Comparator;

public interface ISorter {

  void sort(SimpleList<Contract> list, Comparator<Contract> comparator);
}
