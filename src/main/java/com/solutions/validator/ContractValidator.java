package com.solutions.validator;

import com.solutions.model.contracts.Contract;
import com.solutions.validator.model.ValidResultDto;

public interface ContractValidator {

  ValidResultDto validContract(Contract contract);
}
