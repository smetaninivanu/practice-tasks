package com.solutions.validator.model;

import com.solutions.logic.SimpleList;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ValidResultDto {

  @Setter(value = AccessLevel.NONE)
  private ValidationResult result;

  private SimpleList<String> warnings;

  public ValidResultDto() {
    this.result = ValidationResult.OK;
    this.warnings = new SimpleList<>();
  }

  public void setResult(ValidationResult result) {
    if (this.result.equals(ValidationResult.WARN)) return;

    this.result = result;
  }

  public int size() {
    return warnings.size();
  }

  public boolean add(String warning) {
    result = ValidationResult.WARN;

    return warnings.add(warning);
  }

  public void addAll(SimpleList<String> warnings) {
    if (warnings.size() > 0) result = ValidationResult.WARN;

    this.warnings.addAll(warnings);
  }

  public boolean remove(String warning) {
    boolean res = warnings.remove(warning);

    if (size() == 0) {
      this.result = ValidationResult.OK;
    }

    return res;
  }

  public void clear() {
    result = ValidationResult.OK;

    warnings.clear();
  }

  public String get(int index) {
    return warnings.get(index);
  }

  public String set(int index, String warning) {
    return warnings.set(index, warning);
  }
}
