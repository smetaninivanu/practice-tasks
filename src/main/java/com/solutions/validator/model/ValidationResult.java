package com.solutions.validator.model;

import java.util.Optional;

public enum ValidationResult {
  OK(1),
  WARN(2);

  private final int id;

  ValidationResult(int id) {
    this.id = id;
  }

  public static Optional<ValidationResult> fromId(Integer id) {
    if (id == null) {
      return Optional.empty();
    }

    for (var value : ValidationResult.values()) {
      if (value.id == id) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }

  public int getId() {
    return id;
  }
}
