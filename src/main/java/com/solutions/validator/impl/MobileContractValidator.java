package com.solutions.validator.impl;

import com.solutions.model.contracts.Contract;
import com.solutions.model.contracts.MobileContract;
import com.solutions.validator.ContractValidator;
import com.solutions.validator.model.ValidResultDto;

public class MobileContractValidator implements ContractValidator {

  @Override
  public ValidResultDto validContract(Contract contract) {
    if (!MobileContract.class.equals(contract.getClass())) {
      return new ValidResultDto();
    }

    ValidResultDto resultDto = new ValidResultDto();

    MobileContract mobileContract = (MobileContract) contract;


    if (mobileContract.getMinutes() == 0 &&
        mobileContract.getSms() == 0 &&
        mobileContract.getTraffic() == 0
    ) {
      resultDto.add("minutes, sms and traffic are incorrect;");
    }

    return resultDto;
  }
}
