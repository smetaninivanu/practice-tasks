package com.solutions.validator.impl;

import com.solutions.model.contracts.Contract;
import com.solutions.model.contracts.DigitalTVContract;
import com.solutions.validator.ContractValidator;
import com.solutions.validator.model.ValidResultDto;

public class DigitalTVContractValidator implements ContractValidator {

  @Override
  public ValidResultDto validContract(Contract contract) {
    if (!DigitalTVContract.class.equals(contract.getClass())) {
      return new ValidResultDto();
    }

    ValidResultDto resultDto = new ValidResultDto();

    DigitalTVContract digitalTVContract = (DigitalTVContract) contract;

    if (digitalTVContract.size() == 0) {
      resultDto.add("channel list is empty;");
    }

    return resultDto;
  }
}
