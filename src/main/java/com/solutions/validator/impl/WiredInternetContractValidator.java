package com.solutions.validator.impl;

import com.solutions.model.contracts.Contract;
import com.solutions.model.contracts.WiredInternetContract;
import com.solutions.validator.ContractValidator;
import com.solutions.validator.model.ValidResultDto;

public class WiredInternetContractValidator implements ContractValidator {
  @Override
  public ValidResultDto validContract(Contract contract) {
    if (!WiredInternetContract.class.equals(contract.getClass())) {
      return new ValidResultDto();
    }

    ValidResultDto resultDto = new ValidResultDto();

    WiredInternetContract wiredInternetContract = (WiredInternetContract) contract;


    if (wiredInternetContract.getConnectionSpeed() == 0
    ) {
      resultDto.add("connectionSpeed is incorrect;");
    }

    return resultDto;
  }
}
