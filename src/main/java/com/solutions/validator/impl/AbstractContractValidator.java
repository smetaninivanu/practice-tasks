package com.solutions.validator.impl;

import com.solutions.model.Gender;
import com.solutions.model.Human;
import com.solutions.model.contracts.Contract;
import com.solutions.validator.ContractValidator;
import com.solutions.validator.model.ValidResultDto;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class AbstractContractValidator implements ContractValidator {

  @Override
  public ValidResultDto validContract(Contract contract) {
    ValidResultDto resultDto = new ValidResultDto();

    validOwner(resultDto, contract);
    validContract(resultDto, contract);

    return resultDto;
  }

  private void validOwner(ValidResultDto resultDto, Contract contract) {
    Human owner = contract.getOwner();

    if (owner == null) {
      resultDto.add("owner is null;");
    } else {
      String fullName = owner.getFullName();

      if (fullName == null) {
        resultDto.add("fullName is null;");
      } else if (fullName.isBlank()) {
        resultDto.add("fullName is blank;");
      }

      String passportNumber = owner.getPassportNumber();

      if (passportNumber == null) {
        resultDto.add("passportNumber is null;");
      } else if (passportNumber.isBlank()) {
        resultDto.add("passportNumber is blank;");
      }

      LocalDate birthday = owner.getBirthday();
      int age = owner.getAge();

      if (birthday == null) {
        resultDto.add("birthday is null;");
      } else if (age < 0 || age > 130) {
        resultDto.add("birthday is incorrect;");
      }

      Gender gender = owner.getGender();

      if (gender == null) {
        resultDto.add("gender is null or not equals М, Ж or О;");
      }
    }
  }

  private void validContract(ValidResultDto resultDto, Contract contract) {
    LocalDateTime startDate = contract.getStartDate();

    if (startDate == null) {
      resultDto.add("startDate is null;");
    }

    LocalDateTime endDate = contract.getEndDate();

    if (endDate == null) {
      resultDto.add("endDate is null;");
    }

    if (startDate != null && endDate != null) {
      if (startDate.compareTo(endDate) > 0) {
        resultDto.add("startDate is later than endDate;");
      }
    }

    long contractNumber = contract.getContractNumber();

    if (contractNumber <= 0) {
      resultDto.add("contractNumber is incorrect;");
    }
  }
}
